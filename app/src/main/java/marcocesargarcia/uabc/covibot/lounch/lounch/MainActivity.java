package marcocesargarcia.uabc.covibot.lounch.lounch;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import marcocesargarcia.uabc.covibot.R;
import marcocesargarcia.uabc.covibot.lounch.login.Login;
import marcocesargarcia.uabc.covibot.lounch.modelos.Usuario;
import marcocesargarcia.uabc.covibot.lounch.pantalla_principal.Navegador;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.hide();

        Realm.init(MainActivity.this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("myrealm.realm")
                .schemaVersion(3)
                .deleteRealmIfMigrationNeeded()
                .allowWritesOnUiThread(true)
                .build();
        Realm.setDefaultConfiguration(config);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                // acciones que se ejecutan tras los milisegundos

                Realm realm = Realm.getDefaultInstance();
                Usuario usuario = realm.where(Usuario.class).findFirst();
                if (usuario==null){
                    Intent intent = new Intent(MainActivity.this, Login.class);
                    startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(MainActivity.this).toBundle());
                } else {
                    Intent intent = new Intent(MainActivity.this, Navegador.class);
                    startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(MainActivity.this).toBundle());
                }

            }
        }, 3000);
    }
}