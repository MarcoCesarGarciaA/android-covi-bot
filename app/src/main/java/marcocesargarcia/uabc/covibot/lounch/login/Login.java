package marcocesargarcia.uabc.covibot.lounch.login;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SearchRecentSuggestionsProvider;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import io.realm.Realm;
import marcocesargarcia.uabc.covibot.R;
import marcocesargarcia.uabc.covibot.lounch.MyRestFulGP;
import marcocesargarcia.uabc.covibot.lounch.modelos.Estadisticas;
import marcocesargarcia.uabc.covibot.lounch.modelos.Usuario;
import marcocesargarcia.uabc.covibot.lounch.perfil.RegistroDePerfilActivity;


public class Login extends AppCompatActivity {
    private static int RC_SIGN_IN = 100;
    Button botonLogin;
    private FirebaseAuth mAuth;
    GoogleSignInAccount account;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.hide();

        mAuth = FirebaseAuth.getInstance();

        botonLogin = findViewById(R.id.buttonLogin);
        botonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Configure Google Sign In
                GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestIdToken(getString(R.string.default_web_client_id))
                        .requestEmail()
                        .build();

                GoogleSignInClient signInIntent = GoogleSignIn.getClient(Login.this, gso);
                signInIntent.signOut();
                startActivityForResult(signInIntent.getSignInIntent(), RC_SIGN_IN);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                account = task.getResult(ApiException.class);
                Log.d("FIREBASE LOGIN", "firebaseAuthWithGoogle:" + account.getId());

                //firebaseAuthWithGoogle(account.getIdToken());
                new RegistrarUsuario(getApplicationContext(),account.getDisplayName(),account.getEmail()).execute();
                //new MyAsyncTask(Login.this,account.getDisplayName(),account.getEmail()).execute("GET");
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w("FIREBASE LOGIN", "Google sign in failed", e);
                // ...
            }
        }
    }

    private void firebaseAuthWithGoogle(String idToken) {
        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("FIREBASE LOGIN", "signInWithCredential:success");
                            final FirebaseUser user = mAuth.getCurrentUser();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("FIREBASE LOGIN", "signInWithCredential:failure", task.getException());
                        }
                        // ...
                    }
                });
    }


    //servicio para iniciar sesion
    public class RegistrarUsuario extends AsyncTask<String, Void, String> {

        private Context context;
        String nombre;
        String correo;


        public RegistrarUsuario(Context context, String nombre, String correo) {
            this.context = context;
            this.nombre = nombre;
            this.correo = correo;
        }


        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... arg0) {

            String link;
            String data;
            BufferedReader bufferedReader;
            String result = "";
            try {
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("nombre", nombre));
                params.add(new BasicNameValuePair("correo",correo));

                link = "https://covi-server.000webhostapp.com/Registro.php"; 	///server es el link del servidor
                URL url = new URL(link);
                Log.e("INTERNET", link);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("GET");
                con.setReadTimeout(15000);
                con.setConnectTimeout(15000);
                con.setDoInput(true);
                con.setDoOutput(true);
                OutputStream os = con.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getQuery(params));
                Log.e("INTERNET", writer.toString());
                writer.flush();
                writer.close();
                Log.e("INTERNET", os.toString());
                os.close();

                int responseCode=con.getResponseCode();
                Log.e("INTERNET", String.valueOf(responseCode));

                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
                    while ((line=br.readLine()) != null) {

                        result += line;

                    }
                } else {
                    link = "https://covi-server.000webhostapp.com/Registro.php";
                    url = new URL(link);

                    con = (HttpURLConnection) url.openConnection();
                    con.setRequestMethod("POST");
                    con.setReadTimeout(15000);
                    con.setConnectTimeout(15000);
                    // conn.stRequestMethod("GET");
                    con.setDoInput(true);
                    con.setDoOutput(true);
                    os = con.getOutputStream();
                    writer = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    writer.write(getQuery(params));
                    writer.flush();
                    writer.close();
                    os.close();
                    responseCode = con.getResponseCode();
                    if (responseCode == HttpsURLConnection.HTTP_OK) {
                        String line;
                        BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                        while ((line = br.readLine()) != null) {
                            result += line;
                        }
                    } else {
                        result = "";
                    }
                }
                return result;
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException {
            StringBuilder result = new StringBuilder();
            boolean first = true;

            for (NameValuePair pair : params) {
                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
            }
            Log.e("INTERNET", result.toString());
            return result.toString();
        }

        @Override
        protected void onPostExecute(String result) {  //este método tiene el Json que contesta el servidor
            String jsonStr = result;
            if (jsonStr != null) {
                Log.e("SERVER",jsonStr);
                //if (jsonStr.equals("Usuario creado con éxito") || jsonStr.equals("Sesión iniciada")){
                    //Guardar en la DB
                    Realm realm = Realm.getDefaultInstance();
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            Usuario usuario = realm.createObject(Usuario.class, 1);
                            usuario.setNombre(account.getDisplayName());
                            usuario.setCorreo(account.getEmail());
                            usuario.setImmagenDePerfil(account.getPhotoUrl().toString());
                            usuario.setCiudad("Mexicali");
                            usuario.setEstado("Baja California");

                            //Datos de muestra
                            Estadisticas estadisticas = realm.createObject(Estadisticas.class, 1);
                            estadisticas.setCasos_confirmados(10834);
                            estadisticas.setDefunciones(1743);
                            estadisticas.setSemaforo("rojo");
                            estadisticas.setNombre("Mexicali");
                            Estadisticas estadisticas2 = realm.createObject(Estadisticas.class, 2);
                            estadisticas2.setCasos_confirmados(24207);
                            estadisticas2.setDefunciones(4132);
                            estadisticas2.setSemaforo("naranja");
                            estadisticas2.setNombre("Baja California");


                        }
                    });
                    Intent intent = new Intent(Login.this, RegistroDePerfilActivity.class);
                    startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(Login.this).toBundle());

                /*
                JSONObject jsonObj = new JSONObject(jsonStr);
                //// asi capturas los datos del json —> int query_result = jsonObj.getInt("created");
                if(query_result == 1)
                {
                    Toast.makeText(context, getString(R.string.saved), Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }
                else
                {
                    Toast.makeText(context, "Intentelo mas tarde", Toast.LENGTH_SHORT).show();
                }
                */
            } else {
                Toast.makeText(context, "intentar mas tarde", Toast.LENGTH_SHORT).show();
            }
        }
    }
}