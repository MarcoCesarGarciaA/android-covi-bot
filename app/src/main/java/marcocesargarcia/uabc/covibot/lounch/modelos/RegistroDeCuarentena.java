package marcocesargarcia.uabc.covibot.lounch.modelos;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RegistroDeCuarentena extends RealmObject {
    @PrimaryKey
    int id;
    int idCuarentena;
    Date fecha;
    float temperatura;
    String sintomas;
    int dia_en_cuarentena;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCuarentena() {
        return idCuarentena;
    }

    public void setIdCuarentena(int idCuarentena) {
        this.idCuarentena = idCuarentena;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public float getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(float temperatura) {
        this.temperatura = temperatura;
    }

    public String getSintomas() {
        return sintomas;
    }

    public void setSintomas(String sintomas) {
        this.sintomas = sintomas;
    }

    public int getDia_en_cuarentena() {
        return dia_en_cuarentena;
    }

    public void setDia_en_cuarentena(int dia_en_cuarentena) {
        this.dia_en_cuarentena = dia_en_cuarentena;
    }
}
