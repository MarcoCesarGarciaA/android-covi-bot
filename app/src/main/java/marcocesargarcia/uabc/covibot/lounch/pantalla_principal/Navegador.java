package marcocesargarcia.uabc.covibot.lounch.pantalla_principal;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import marcocesargarcia.uabc.covibot.R;
import marcocesargarcia.uabc.covibot.lounch.modelos.Usuario;

public class Navegador extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navegador);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.hide();

        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_chatbot)
                .build();

        TextView textViewnivelriesgo = findViewById(R.id.texttextViewtextViewNaNivelRiesgo);
        TextView textViewnombre = findViewById(R.id.texttextViewtextViewNaNombre);
        CircleImageView imageViewFoto = findViewById(R.id.imageView3);

        Realm realm = Realm.getDefaultInstance();
        Usuario usuario = realm.where(Usuario.class).findFirst();
        textViewnivelriesgo.setText("Nivel de riesgo de COVID-19: " + usuario.getNivelDeRiesgo());
        textViewnombre.setText(usuario.getNombre());
        Glide.with(this).load(usuario.getImmagenDePerfil()).into(imageViewFoto);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
    }

}