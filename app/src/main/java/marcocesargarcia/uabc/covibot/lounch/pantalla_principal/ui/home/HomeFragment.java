package marcocesargarcia.uabc.covibot.lounch.pantalla_principal.ui.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;


import io.realm.Realm;
import marcocesargarcia.uabc.covibot.R;
import marcocesargarcia.uabc.covibot.lounch.modelos.Estadisticas;
import marcocesargarcia.uabc.covibot.lounch.modelos.Usuario;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private LinearLayout linearLayout;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        linearLayout = root.findViewById(R.id.linearleyouthome);

        //Registro de prueba realm
        Realm realm = Realm.getDefaultInstance();
        Usuario usuario = realm.where(Usuario.class).equalTo("id",1).findFirst();
        Estadisticas ciudad = realm.where(Estadisticas.class).equalTo("nombre",usuario.getCiudad()).findFirst();
        Estadisticas estado = realm.where(Estadisticas.class).equalTo("nombre",usuario.getEstado()).findFirst();
        realm.close();


        //agregamos las tarjetas al list_view
        añadirRegistroDeCuarentena(usuario.getFactorDeRiesgo(),usuario.getNivelDeRiesgo(),usuario.getPadecimientos());
        añadirSemaforo(ciudad.getNombre(),ciudad.getCasos_confirmados(),ciudad.getDefunciones(),ciudad.getSemaforo());
        añadirSemaforoEstado(estado.getNombre(),estado.getCasos_confirmados(),estado.getDefunciones(),estado.getSemaforo());
        añadirSemaforoNacional(435897897,440594,"rojo");
        añadirRecomendacionesMedicas("No toques tu cara, ojos, boca o nariz",
                "No te automediques",
                "Quedate en casa",
                "No saludes de mano, beso o abrazo y manten tu sana distancia.",
                "Al toser o estornudar, tapate con un pañuelo desechable o el angulo interno del codo.",
                "Lavate las manos frecuentemente con agua y jabon, al menos 20 segundos o desinfectarlas con gel antibacterial.",
                "Si debes salir recuerda las medidas de sana distancia (2 metros entre persona y persona).",
                "Haz ejercicio en casa.",
                "Alimentate sanamente","No utilizar dioxido de cloro");
        añadirCuidadosEnfermos("Si presenta sintomas, cubre boca y nariz con cubrebocas y acuda de inmediato al medico, hospital o clinica mas cercano.",
                " Si no presenta sintomas,  quedase en su casa."
                , "Laven vasos y cubierto con agua, jabon y cloro.");
        añadirCuidadosEmocionales("Mantente ocupado en actividades productivas y positivas, comparte tiempo con tu familia, colabora en las tareas del hogar , practica la tolerancia , solidaridad y respecto.",
                "Si cree que necesitas algun consejo o apoyo, llame a la linea de la vida: 800 911 2000.");
        añadirCuidadosMaternos("Procura salir unicamente tus consultas medicas programadas.",
                "Conocer las señales de alarma en el embarazo y solicitar atencion si presenta alguno de ellos.",
                "Importante que la persona que te acompañe en esta etapa, este sana y tambien permanezca en casa.",
                "Si es posible, pida ayuda para que alguien mas realize las compras.",
                "Mantener en comunicacion con tus familiares por telefono, !sentirte acompañada en esta etapa es muy importante!.");
        añadirCuidadosAdultosMayores("Come sanamente: frutas, verduras y agua. Anda de refrescos y comida chatarra o con gluten.",
                "No consuma alcohol ni tabaco.",
                "Si padece de diabetes o hipertension, checate el nivel de glucosa, presion sanguinea y temperatura cada dia.",
                "Por ahora, saludar de lejitos es mejor, nada de a beso, mano ni abrazos.",
                "Evita el miedo, realizando actividades que te gusten; leer, escuchar musica, escribir, tejer o hacer un rompecabeza.","Evita el miedo, realizando actividades que te gusten; leer, escuchar musica, escribir, tejer o hacer un rompecabeza.",
                "Designe a alguien de confianza si necesitas ayuda , hacer compras o ir al medico.");
        return root;
    }

    private void añadirSemaforo(String ciudad, int casosConfirmados, int defunciones, String semaforo) {
        View cardviewSemaforo = View.inflate(getContext(),R.layout.row_estadistica_semaforo,linearLayout);

        ImageView imageViewsemaforo = cardviewSemaforo.findViewById(R.id.imageViewSemaforo);
        TextView textViewCiudad = cardviewSemaforo.findViewById(R.id.textViewCiudad);
        TextView textViewCasosconfirmados = cardviewSemaforo.findViewById(R.id.textViewConfirmados);
        TextView textViewDefunciones = cardviewSemaforo.findViewById(R.id.textViewDefuncion);

        textViewCiudad.setText(ciudad);
        textViewCasosconfirmados.setText("Casos confirmados: " + casosConfirmados);
        textViewDefunciones.setText("Casos defunción: " + defunciones);

        switch (semaforo) {
            case "rojo":
                imageViewsemaforo.setImageDrawable(getContext().getDrawable(R.drawable.ic_semaforo_rojo));
                break;
            case "naranja":
                imageViewsemaforo.setImageDrawable(getContext().getDrawable(R.drawable.ic_semaforo_naranja));
                break;
            case "verde":
                imageViewsemaforo.setImageDrawable(getContext().getDrawable(R.drawable.ic_semaforo_verde));
                break;
        }
    }

    private void añadirSemaforoEstado(String estado, int casosConfirmados, int defunciones, String semaforo) {
        View cardviewSemaforo = View.inflate(getContext(),R.layout.row_estadistica_semaforo_estado,linearLayout);
        ImageView imageViewsemaforo = cardviewSemaforo.findViewById(R.id.imageViewSemaforoEstado);
        TextView textViewCiudad = cardviewSemaforo.findViewById(R.id.textViewEstado);
        TextView textViewCasosconfirmados = cardviewSemaforo.findViewById(R.id.textViewConfirmadosEstado);
        TextView textViewDefunciones = cardviewSemaforo.findViewById(R.id.textViewDefuncionEstado);

        textViewCiudad.setText(estado);
        textViewCasosconfirmados.setText("Casos confirmados: " + casosConfirmados);
        textViewDefunciones.setText("Casos defunción: " + defunciones);

        switch (semaforo) {
            case "rojo":
                imageViewsemaforo.setImageDrawable(getContext().getDrawable(R.drawable.ic_semaforo_rojo));
                break;
            case "naranja":
                imageViewsemaforo.setImageDrawable(getContext().getDrawable(R.drawable.ic_semaforo_naranja));
                break;
            case "verde":
                imageViewsemaforo.setImageDrawable(getContext().getDrawable(R.drawable.ic_semaforo_verde));
                break;
        }
    }

    private void añadirRegistroDeCuarentena(String factorDeRiesgo, int nivelDeRiesgo, String padecimiento) {
        View cardviewSemaforo = View.inflate(getContext(),R.layout.row_registro_de_cuarentena,linearLayout);

        TextView textViewNivelRiesgo = cardviewSemaforo.findViewById(R.id.textViewNR);
        TextView textViewFactorRiesgo = cardviewSemaforo.findViewById(R.id.textViewFR);
        TextView textViewPadecimiento = cardviewSemaforo.findViewById(R.id.textViewPU);

        textViewNivelRiesgo.setText("Nivel de Riesgo: " + nivelDeRiesgo);
        textViewFactorRiesgo.setText("Factor de Riesgo: " + factorDeRiesgo);
        textViewPadecimiento.setText("Padecimientos de Usuario: " + padecimiento);
    }

    private void añadirSemaforoNacional(int casosConfirmados, int defunciones, String semaforo) {
        View cardviewSemaforo = View.inflate(getContext(),R.layout.row_estadistica_semaforo_nacional,linearLayout);

        ImageView imageViewsemaforo = cardviewSemaforo.findViewById(R.id.imageViewSemaforoNacional);
        TextView textViewCasosconfirmados = cardviewSemaforo.findViewById(R.id.textViewConfirmadosNacional);
        TextView textViewDefunciones = cardviewSemaforo.findViewById(R.id.textViewDefuncionNacional);

        textViewCasosconfirmados.setText("Casos confirmados: " + casosConfirmados);
        textViewDefunciones.setText("Casos defunción: " + defunciones);

        switch (semaforo) {
            case "rojo":
                imageViewsemaforo.setImageDrawable(getContext().getDrawable(R.drawable.ic_semaforo_rojo));
                break;
            case "naranja":
                imageViewsemaforo.setImageDrawable(getContext().getDrawable(R.drawable.ic_semaforo_naranja));
                break;
            case "verde":
                imageViewsemaforo.setImageDrawable(getContext().getDrawable(R.drawable.ic_semaforo_verde));
                break;
        }
    }

    private void añadirRecomendacionesMedicas(String R1, String R2, String R3, String R4, String R5, String R6, String R7, String R8, String R9, String R10) {
        View cardviewSemaforo = View.inflate(getContext(),R.layout.row_recomendaciones_medicas,linearLayout);

        TextView textViewR1 = cardviewSemaforo.findViewById(R.id.textViewR1);
        TextView textViewR2 = cardviewSemaforo.findViewById(R.id.textViewR2);
        TextView textViewR3 = cardviewSemaforo.findViewById(R.id.textViewR3);
        TextView textViewR4 = cardviewSemaforo.findViewById(R.id.textViewR4);
        TextView textViewR5 = cardviewSemaforo.findViewById(R.id.textViewR5);
        TextView textViewR6 = cardviewSemaforo.findViewById(R.id.textViewR6);
        TextView textViewR7 = cardviewSemaforo.findViewById(R.id.textViewR7);
        TextView textViewR8 = cardviewSemaforo.findViewById(R.id.textViewR8);
        TextView textViewR9 = cardviewSemaforo.findViewById(R.id.textViewR9);
        TextView textViewR10 = cardviewSemaforo.findViewById(R.id.textViewR10);

        textViewR1.setText(R1);
        textViewR2.setText(R2);
        textViewR3.setText(R3);
        textViewR4.setText(R4);
        textViewR5.setText(R5);
        textViewR6.setText(R6);
        textViewR7.setText(R7);
        textViewR8.setText(R8);
        textViewR9.setText(R9);
        textViewR10.setText(R10);
    }
    private void añadirCuidadosEnfermos(String CEN1, String CEN2, String CEN3) {
        View cardviewSemaforo = View.inflate(getContext(),R.layout.row_cuidados_enfermos,linearLayout);

        TextView textViewCEN1 = cardviewSemaforo.findViewById(R.id.textViewCEN1);
        TextView textViewCEN2 = cardviewSemaforo.findViewById(R.id.textViewCEN2);
        TextView textViewCEN3 = cardviewSemaforo.findViewById(R.id.textViewCEN3);

        textViewCEN1.setText(CEN1);
        textViewCEN2.setText(CEN2);
        textViewCEN3.setText(CEN3);
    }
    private void añadirCuidadosEmocionales(String CE1, String CE2) {
        View cardviewSemaforo = View.inflate(getContext(),R.layout.row_cuidados_emocionales,linearLayout);

        TextView textViewCE1 = cardviewSemaforo.findViewById(R.id.textViewCE1);
        TextView textViewCE2 = cardviewSemaforo.findViewById(R.id.textViewCE2);

        textViewCE1.setText(CE1);
        textViewCE2.setText(CE2);
    }
    private void añadirCuidadosMaternos(String CM1, String CM2, String CM3, String CM4, String CM5) {
        View cardviewSemaforo = View.inflate(getContext(),R.layout.row_cuidados_maternos,linearLayout);

        TextView textViewCM1 = cardviewSemaforo.findViewById(R.id.textViewCM1);
        TextView textViewCM2 = cardviewSemaforo.findViewById(R.id.textViewCM2);
        TextView textViewCM3 = cardviewSemaforo.findViewById(R.id.textViewCM3);
        TextView textViewCM4 = cardviewSemaforo.findViewById(R.id.textViewCM4);
        TextView textViewCM5 = cardviewSemaforo.findViewById(R.id.textViewCM5);

        textViewCM1.setText(CM1);
        textViewCM2.setText(CM2);
        textViewCM3.setText(CM3);
        textViewCM4.setText(CM4);
        textViewCM5.setText(CM5);
    }
    private void añadirCuidadosAdultosMayores(String CAM1, String CAM2, String CAM3, String CAM4, String CAM5, String CAM6, String CAM7) {
        View cardviewSemaforo = View.inflate(getContext(),R.layout.row_cuidados_adultos_mayores,linearLayout);

        TextView textViewCAM1 = cardviewSemaforo.findViewById(R.id.textViewCAM1);
        TextView textViewCAM2 = cardviewSemaforo.findViewById(R.id.textViewCAM2);
        TextView textViewCAM3 = cardviewSemaforo.findViewById(R.id.textViewCAM3);
        TextView textViewCAM4 = cardviewSemaforo.findViewById(R.id.textViewCAM4);
        TextView textViewCAM5 = cardviewSemaforo.findViewById(R.id.textViewCAM5);
        TextView textViewCAM6 = cardviewSemaforo.findViewById(R.id.textViewCAM4);
        TextView textViewCAM7 = cardviewSemaforo.findViewById(R.id.textViewCAM5);

        textViewCAM1.setText(CAM1);
        textViewCAM2.setText(CAM2);
        textViewCAM3.setText(CAM3);
        textViewCAM4.setText(CAM4);
        textViewCAM5.setText(CAM5);
        textViewCAM6.setText(CAM6);
        textViewCAM7.setText(CAM7);

    }
}