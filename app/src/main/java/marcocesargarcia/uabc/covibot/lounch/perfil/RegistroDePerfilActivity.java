package marcocesargarcia.uabc.covibot.lounch.perfil;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.List;

import io.realm.Realm;
import marcocesargarcia.uabc.covibot.R;
import marcocesargarcia.uabc.covibot.lounch.modelos.Usuario;
import marcocesargarcia.uabc.covibot.lounch.pantalla_principal.Navegador;

public class RegistroDePerfilActivity extends AppCompatActivity implements MultiSelectionSpinner.OnMultipleItemsSelectedListener {
    private Button botonSalvar;
    private Spinner spinnerSexo;
    private MultiSelectionSpinner multiSelectionArraySpinner;
    private Spinner spinnerSituacionPeso;
    private EditText editTextEdad;

    private String sexo = "";
    private String sitacionPeso = "";
    private String padecimiento = "";
    private String edad = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_de_perfil);
        multiSelectionArraySpinner = findViewById(R.id.spinner_string_array);
        spinnerSexo = findViewById(R.id.spinner);
        spinnerSituacionPeso = findViewById(R.id.spinner2);
        editTextEdad = findViewById(R.id.editTextNumber2);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.hide();

        //Multiarray
        padecimiento = "Hipertension";
        String[] array = {"Hipertension", "Diabetes", "EPOC", "Enfermedades Renal Cronica", "Inmunosupresion"};
        multiSelectionArraySpinner.setItems(array);
        multiSelectionArraySpinner.setListener(this);

        //Boton salvar
        botonSalvar = findViewById(R.id.buttonGuardarRegistro);
        botonSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //revisar valores
                sexo = spinnerSexo.getSelectedItem().toString();
                edad = editTextEdad.getText().toString();
                sitacionPeso = spinnerSituacionPeso.getSelectedItem().toString();

                if (!sexo.equals("") && !edad.equals("") && !sitacionPeso.equals("")) {
                    Realm realm = Realm.getDefaultInstance();
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            Usuario usuario = realm.where(Usuario.class).findFirst();
                            usuario.setSexo(sexo);
                            usuario.setEdad(Integer.parseInt(edad));
                            usuario.setPeso(sitacionPeso);
                            usuario.setPadecimientos(padecimiento);
                        }
                    });
                    Intent intent = new Intent(RegistroDePerfilActivity.this, Navegador.class);
                    startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(RegistroDePerfilActivity.this).toBundle());
                }
            }
        });
    }

    @Override
    public void selectedIndices(List<Integer> indices, MultiSelectionSpinner spinner) {

    }

    @Override
    public void selectedStrings(List<String> strings, MultiSelectionSpinner spinner) {
        padecimiento = strings.toString().substring(1, strings.toString().length() - 1);
    }
}