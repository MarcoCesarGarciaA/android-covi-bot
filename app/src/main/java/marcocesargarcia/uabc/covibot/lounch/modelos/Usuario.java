package marcocesargarcia.uabc.covibot.lounch.modelos;

import android.net.Uri;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Usuario extends RealmObject {
    @PrimaryKey
    private int id;
    private String nombre;
    private String ciudad;
    private String estado;
    private String ubicacion;
    private String correo;
    private String sexo;
    private String peso;
    private String immagenDePerfil;
    private int edad;
    private String padecimientos;
    private int nivelDeRiesgo;
    private String factorDeRiesgo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getImmagenDePerfil() {
        return immagenDePerfil;
    }

    public void setImmagenDePerfil(String immagenDePerfil) {
        this.immagenDePerfil = immagenDePerfil;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public int getNivelDeRiesgo() {
        return nivelDeRiesgo;
    }

    public void setNivelDeRiesgo(int nivelDeRiesgo) {
        this.nivelDeRiesgo = nivelDeRiesgo;
    }

    public String getPadecimientos() {
        return padecimientos;
    }

    public void setPadecimientos(String padecimientos) {
        this.padecimientos = padecimientos;
    }

    public String getFactorDeRiesgo() {
        return factorDeRiesgo;
    }

    public void setFactorDeRiesgo(String factorDeRiesgo) {
        this.factorDeRiesgo = factorDeRiesgo;
    }
}
