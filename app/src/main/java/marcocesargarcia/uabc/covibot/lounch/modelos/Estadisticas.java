package marcocesargarcia.uabc.covibot.lounch.modelos;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Estadisticas extends RealmObject {
    @PrimaryKey
    private int id;
    private String nombre;
    private int casos_negativos;
    private int casos_sospechosos;
    private int casos_confirmados;
    private int defunciones;
    private int recuperados;
    private int casos_activos;
    private String semaforo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCasos_negativos() {
        return casos_negativos;
    }

    public void setCasos_negativos(int casos_negativos) {
        this.casos_negativos = casos_negativos;
    }

    public int getCasos_sospechosos() {
        return casos_sospechosos;
    }

    public void setCasos_sospechosos(int casos_sospechosos) {
        this.casos_sospechosos = casos_sospechosos;
    }

    public int getCasos_confirmados() {
        return casos_confirmados;
    }

    public void setCasos_confirmados(int casos_confirmados) {
        this.casos_confirmados = casos_confirmados;
    }

    public int getDefunciones() {
        return defunciones;
    }

    public void setDefunciones(int defunciones) {
        this.defunciones = defunciones;
    }

    public int getRecuperados() {
        return recuperados;
    }

    public void setRecuperados(int recuperados) {
        this.recuperados = recuperados;
    }

    public int getCasos_activos() {
        return casos_activos;
    }

    public void setCasos_activos(int casos_activos) {
        this.casos_activos = casos_activos;
    }

    public String getSemaforo() {
        return semaforo;
    }

    public void setSemaforo(String semaforo) {
        this.semaforo = semaforo;
    }
}
